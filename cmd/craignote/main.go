package main

import (
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	bolt "go.etcd.io/bbolt"
)

type Query struct {
	URL   string
	Items []Item `xml:"item"`
}

type Item struct {
	Item  xml.Name
	Title string `xml:"title"`
	Link  string `xml:"link"`
	Desc  string `xml:"description"`
}

func main() {
	flag.Parse()
	if flag.NArg() < 1 {
		fmt.Fprintln(os.Stderr, "Missing argument.")
		os.Exit(1)
	}

	var err error
	switch cmd := flag.Arg(0); cmd {
	case "add":
		if err := add(flag.Args()[1]); err != nil {
			fmt.Fprintf(os.Stderr, "Failed to add url %s: %v", flag.Args()[1], err)
			os.Exit(1)
		}
	case "remove":
		if err := del(flag.Args()[1]); err != nil {
			fmt.Fprintf(os.Stderr, "Failed to remove query '%s': %v", flag.Args()[1], err)
			os.Exit(1)
		}
	case "list":
		l, err := list()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not list queries: %v", err)
			os.Exit(1)
		}
		fmt.Println(l)
	default:
		err = fmt.Errorf("Unknown command %s", cmd)
		os.Exit(1)
	}
	if err != nil {
		fmt.Fprint(os.Stderr, err)
		os.Exit(1)
	}
}

func getXML(url string) string {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error on GET to url '%s': %s/n", url, err)
	}
	defer resp.Body.Close()

	b, _ := ioutil.ReadAll(resp.Body)
	x := string(b)
	return x
}

func (q *Query) updateQuery() error {
	rssURL := fmt.Sprintf("%s&format=rss", q.URL)
	resp, err := http.Get(rssURL)
	if err != nil {
		return fmt.Errorf("Failed to open URL '%s': %v", q.URL, err)
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("failed to read body of response: %v", err)
	}

	xml.Unmarshal(b, q)
	return nil
}

func add(url string) error {
	db, err := bolt.Open("c.db", 0600, nil)
	if err != nil {
		return fmt.Errorf("failed to open the database: %v", err)
	}
	defer db.Close()

	q := Query{}
	q.URL = url
	q.updateQuery()
	itemsJson, err := json.Marshal(q)
	if err != nil {
		return fmt.Errorf("failed to marshal query: %s", err)
	}

	err = db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("queries"))
		if err != nil {
			return fmt.Errorf("could not create bucket queries: %v", err)
		}

		err = b.Put([]byte(q.URL), itemsJson)
		if err != nil {
			return fmt.Errorf("could not create bucket queries: %v", err)
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("could not update the database: %v", err)
	}
	return nil
}

func del(url string) error {
	db, err := bolt.Open("c.db", 0600, nil)
	if err != nil {
		return fmt.Errorf("failed to open the database: %v", err)
	}
	defer db.Close()

	err = db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("queries"))
		if err != nil {
			return fmt.Errorf("could not create/open bucket: %v", err)
		}

		err = b.Delete([]byte(url))
		if err != nil {
			return fmt.Errorf("could not remove key: %v", err)
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("failed to remove key from database: %v", err)
	}
	return nil
}

func list() (string, error) {
	db, err := bolt.Open("c.db", 0600, nil)
	if err != nil {
		return "", fmt.Errorf("failed to open the database: %v", err)
	}
	defer db.Close()

	var bld strings.Builder
	bld.WriteString("\n")

	err = db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("queries"))
		if b == nil {
			return nil
		}
		if err != nil {
			return fmt.Errorf("could not create bucket queries: %v", err)
		}

		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			q := Query{}
			if err = json.Unmarshal(v, &q); err != nil {
				return fmt.Errorf("could not unmarshal %s, %v", string(v), err)
			}
			bld.WriteString(fmt.Sprintf("%s\n", string(k)))
			for _, i := range q.Items {
				bld.WriteString(fmt.Sprintf("\t→ %s\n", i.Title))
			}
			bld.WriteString("\n")
		}
		return nil
	})
	if err != nil {
		return "", fmt.Errorf("could not list database: %v", err)
	}

	return bld.String(), nil
}
